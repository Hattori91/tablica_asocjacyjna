#include <iostream>
#include <fstream>
#include <cstdlib>
#include <time.h>
#include <list>
#include <string>

using namespace std;



struct struktura{

	char *klucz;
	int wartosc;

};


class Tablica{

public:


	struktura dane;
	list<struktura> lista;
	char *tmp;

	int dodaj(char*, int);
	void usun(char*);
	int pobierz(char*);
	bool czyPusta();
	int zliczElementy();

	int& operator[](char*);
	Tablica& operator=(int);								//Przeciazenie operatora przypisania
	
	/*
	friend ostream & operator<< (ostream&,Tablica&);			//Przeciazenie operatora wyjscia
	Tablica& operator= (Tablica&);								//Przeciazenie operatora przypisania
	bool operator== (Tablica&);									//Przeciazenie operatora porownania
	*/




};


int& Tablica::operator[](char *b)
{
	 int i;
	 tmp = b;
	 return i=this->pobierz(b);

}

Tablica& Tablica::operator=(int  b)
{
	int i;
	i = dodaj(this->tmp, b);
	return *this;
}

int Tablica::dodaj(char *nazwa, int wartosc)
{

	for (list<struktura>::iterator iter = lista.begin(); iter != lista.end(); iter++)
	if (strcmp(nazwa, iter->klucz) == 0)
	{
		iter->wartosc = wartosc;
		return iter->wartosc;
	}

	dane.wartosc = wartosc;
	dane.klucz = nazwa;
	lista.push_back(dane);
	return dane.wartosc;

}

void Tablica::usun(char *nazwa)
{
	for (list<struktura>::iterator iter = lista.begin(); iter != lista.end();)

	if (strcmp(nazwa, iter->klucz) == 0)
		iter = lista.erase(iter);
	else
		iter++;


}


int Tablica::pobierz(char *nazwa)
{
	for (list<struktura>::iterator iter = lista.begin(); iter != lista.end(); iter++)
	if (strcmp(nazwa, iter->klucz) == 0)
		return iter->wartosc;

	return -1;
	
	
}


bool Tablica::czyPusta()
{
	return lista.empty();
}


int Tablica::zliczElementy()
{
	return lista.size();
}


int main(void)
{
	Tablica tab;

	//cout << tab.czyPusta() << endl;        //zwraca 1 gdy pusta tablica

	tab.dodaj("Godyla", 54);                //dodaje wartosc 54 do tablicy o indexie "Godyla"


	cout << tab["Godyla"] << endl;

	/*tab.dodaj("Godyla", 13);                //nadpisuje komorke o podanej wartosci

	cout << tab.pobierz("Godyla") << endl;

	cout << tab.zliczElementy() << endl;  //Liczy ilosc elementow tablicy

	tab.usun("Godyla");

	cout << tab.czyPusta() << endl;*/



	system("Pause");
	return 0;
}
